@extends('layout.master')

@section('title','Jadwal Fosti | Sabtu')

@section('intro')
    <script src="{{ asset('js/highcharts.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style_jadwal.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=0.5">

    <script>
        $(function () {
            var chart;
            $(document).ready(function () {
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'container',
                        type: 'column',
                        marginRight: 130,
                        marginBottom: 30
                    },
                    title: {
                        text: 'Grafik Jadwal Anggota FOSTI 2016',
                        x: -20
                    },
                    subtitle: {
                        text: 'FORUM OPEN SOURCE TEKNIK INFORMATIKA',
                        x: -20
                    },
                    xAxis: {
                        categories: ['Jam Ke-1', 'Jam Ke-2', 'Jam Ke-3', 'Jam Ke-4', 'Jam Ke-5', 'Jam Ke-6', 'Jam Ke-7', 'Jam Ke-8', 'Jam Ke-9', 'Jam Ke-10', 'Jam Ke-11', 'Jam Ke-12', 'Jam Ke-13', 'Jam Ke-14', 'Jam Ke-15', 'Jam Ke-16']
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Orang'
                        },
                        plotcolumns: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.series.name + '</b><br/>' +
                                this.x + ': ' + this.y;
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -10,
                        y: 100,
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Orang',
                        data: [{{ $ke1 }},{{ $ke2 }},{{ $ke3 }},{{ $ke4 }},{{ $ke5 }},{{ $ke6 }},{{ $ke7 }},{{ $ke8 }},{{ $ke9 }},{{ $ke10 }},{{ $ke11 }},{{ $ke12 }},{{ $ke13 }},{{ $ke14 }},{{ $ke15 }},{{ $ke16 }}]
                    }]
                });
            });

        });
    </script>
@endsection

@section('content')
    <div class="container main" style="margin-top:10px">
        <div class="panel panel-default">
            <div class="panel-body">
                <!--grafik akan ditampilkan disini -->
                <div id="container"></div>
            </div>
        </div>
        <div style="padding-left: 20px">
            <input data-toggle="tooltip" title="Total semua yang telah mengirim jadwal" type="button"
                   value="Pengirim : {{ $total }} orang" class="btn btn-default">
        </div>
        <center>
            <br>
            <a href="{{ action('ShowJadwalController@semua') }}" class="btn btn-default">Semua</a>
            <br><br>
            <a href="{{ action('ShowJadwalController@senin') }}" class="btn btn-default">Senin</a>
            <a href="{{ action('ShowJadwalController@selasa') }}" class="btn btn-default">Selasa</a>
            <a href="{{ action('ShowJadwalController@rabu') }}" class="btn btn-default">Rabu</a>
            <a href="{{ action('ShowJadwalController@kamis') }}" class="btn btn-default">Kamis</a>
            <a href="{{ action('ShowJadwalController@jumat') }}" class="btn btn-default">Jumat</a>
            <a href="#" class="btn btn-primary">Sabtu</a>
        </center>
    </div>
@endsection