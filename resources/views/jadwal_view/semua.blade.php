@extends('layout.master')

@section('title','Jadwal Fosti | Seminggu')

@section('intro')
    <script src="{{ asset('js/highcharts.js')}}"></script>
    <meta name="viewport" content="width=device-width, initial-scale=0.5">

    <script>
        $(function () {
            var chart;
            $(document).ready(function () {
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'container',
                        type: 'column',
                        marginRight: 130,
                        marginBottom: 30
                    },
                    title: {
                        text: 'Grafik Jadwal Anggota FOSTI 2016',
                        x: -20
                    },
                    subtitle: {
                        text: 'FORUM OPEN SOURCE TEKNIK INFORMATIKA',
                        x: -20
                    },
                    xAxis: {
                        categories: ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Orang'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.series.name + '</b><br/>' +
                                this.x + ': ' + this.y;
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -10,
                        y: 100,
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Orang',
                        data: [{{ $senin }},{{ $selasa }},{{ $rabu }},{{ $kamis }},{{ $jumat }}, {{ $sabtu }}]
                    }]
                });
            });

        });
    </script>
@endsection

@section('content')
    <div class="container main" style="margin-top:10px">
        <div class="panel panel-default">
            <div class="panel-body">
                <!--grafik akan ditampilkan disini -->
                <div id="container"></div>
            </div>
        </div>
        <div style="padding-left: 20px">
            <input data-toggle="tooltip" title="Total semua yang telah mengirim jadwal" type="button"
                   value="Pengirim : {{ $total }} orang" class="btn btn-default">
        </div>
        <center>
            <br>
            <a href="#" class="btn btn-primary" style="width: 100px;">Semua</a>
            <br><br>
            <a href="{{ action('ShowJadwalController@senin') }}" class="btn btn-default" style="width: 100px;">Senin</a>
            <a href="{{ action('ShowJadwalController@selasa') }}" class="btn btn-default"
               style="width: 100px;">Selasa</a>
            <a href="{{ action('ShowJadwalController@rabu') }}" class="btn btn-default" style="width: 100px;">Rabu</a>
            <a href="{{ action('ShowJadwalController@kamis') }}" class="btn btn-default" style="width: 100px;">Kamis</a>
            <a href="{{ action('ShowJadwalController@jumat') }}" class="btn btn-default" style="width: 100px;">Jumat</a>
            <a href="{{ action('ShowJadwalController@senin') }}" class="btn btn-default" style="width: 100px;">Sabtu</a>
        </center>
    </div>
@endsection