<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <link href="{{ asset('images/icn.png') }}" rel="shortcut icon" type="image/x-icon">
    
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- font awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <!-- sweetalert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
    <!-- semantic ui -->
    <link href="{{ asset('semantic/semantic.min.css') }}" rel="stylesheet" type="text/css">
    <!-- font lato -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <style>
        html,
        body {
            margin: 0;
            padding: 0;
            height: 100%;
            background: url("{{ asset('images/bg.png')  }}");
        }

        #wrapper {
            min-height: 100%;
            position: relative;
        }

        #header {
            background: #ededed;
            padding: 10px;
        }

        #content {
            padding-bottom: 80px;
        }

        #footer {
            width: 100%;
            position: absolute;
            bottom: 0;
            left: 0;
        }

        .btn {
            border-radius: 0px;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
        }

    </style>
    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- sweetalert -->
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    
    @yield('intro')

    <!-- bootsrap.min.js -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Semantic -->   
    <script src="{{ asset('semantic/semantic.min.js') }}"></script>


</head>
<body>


<div id="wrapper">
    <div id="content">
        @yield('content')
    </div>
<!--     <center>
        <div id="footer" style="color: #FFFFFF; background-color: #337AB7; padding-top: 5px; padding-bottom: 5px">
            <small>Powered by Forum Open Source Teknik Informatika <br> Universitas Muhammadiyah Surakarta</small>
        </div>
    </center> -->

</div>

@yield('add_bot_js')

</body>

</html>