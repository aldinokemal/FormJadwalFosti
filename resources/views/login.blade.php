@extends('layout.master')
@section('title', 'Log In | Jadwal Fosti ')
@section('intro')
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">
@endsection
@section('content')

    <link rel="stylesheet" type="text/css" href="{{ asset('css/style_login.css') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <div class="container">
        <div class="col-md-3"></div>
        <div class="col-md-6">

            <div class="panel panel-primary custom-flat">
                <div class="panel-heading custom-flat"><h3><i class="fa fa-calendar-check-o" aria-hidden="true"
                                                              style="font-size:20px;"></i> Form Jadwal</h3></div>
                <div class="panel-body">
                    <div class="alert alert-active" style="background-color: #F7F7F7">

                        <form method="post" action="{{ action('LoginController@proses') }}"
                              class="form-horizontal uk-form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="id-nim" class="col-sm-2 control-label">
                                <span class="label-rounded"><span class="glyphicon glyphicon-user"
                                                                  aria-hidden="true"></span></span>
                                </label>
                                <div class="col-sm-10 {{ $errors->has('user') ? 'has-error' : '' }}" >
                                    <input type="text" name="user" class="form-control custom-flat" placeholder="Username" id="id-nim" value="{{ old('user') }}" {{ old('user') ? '' : 'autofocus' }}>
                                    @if ($errors->has('user'))
                                        <font color='#A52D25' style="float: left;"><span
                                                class="glyphicon glyphicon-exclamation-sign"
                                                aria-hidden="true"></span><strong> Please enter username</strong></font>
                                    @endif
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label for="id-password" class="col-sm-2 control-label">
                                <span class="label-rounded"><span class="glyphicon glyphicon-lock"
                                                                  aria-hidden="true"></span></span>
                                </label>
                                <div class="col-sm-10 {{ $errors->has('user') ? 'has-error' : '' }}">
                                    <div class="input-group">
                                        <input type="password" id="pwd" name="pwd" class="form-control custom-flat" placeholder="Password" {{ old('user') ? 'autofocus' : '' }}>
                                        <span class="input-group-addon" id="span_eye_icon" style="cursor:pointer;" onclick="unhidePassword();"><span id="eye_icon" class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></span>
                                    </div>
                                    @if ($errors->has('pwd'))
                                        <font color='#A52D25' style="float: left;"><span
                                                    class="glyphicon glyphicon-exclamation-sign"
                                                    aria-hidden="true"></span><b> Please enter password</b></font>
                                    @endif
                                </div>
                            </div>
                            <center>
                                <div class="form-group">
                                    <button type="submit" name="login" class="btn btn-primary custom-flat">
                                        <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Sign In
                                    </button>
                                    <a href="{{action('ShowJadwalController@semua')}}" target='_blank'
                                       class="btn btn-default custom-flat">
                                        <i class="fa fa-bar-chart"></i> Chart
                                    </a>
                                </div>
                            </center>
                        </form>
                        @if (session()->has('error'))
                            <div class="alert_manual">
                                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"
                                      style="color: #A52D25"></span>
                                <font color='#A52D25'><b>{{ session('error') }}</b></font>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>

    </div>

@section('add_bot_js')
    <script type="text/javascript">
        function unhidePassword(){
            $("#eye_icon").attr('style', 'color : #3184CC');
            $("#span_eye_icon").attr("onclick","hidePassword()");
            $("#pwd").attr("type","text");
        }
        function hidePassword(){
            $("#eye_icon").attr('style', 'color : none');
            $("#span_eye_icon").attr("onclick","unhidePassword()");
            $("#pwd").attr("type","password");
        }
    </script>
@endsection

@endsection