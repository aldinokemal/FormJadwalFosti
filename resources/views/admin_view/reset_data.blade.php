@extends('layout.master')

@section('title','Admin | Reset Data')

@section('intro')

@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href={{asset('css/style_login.css')}}>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <center>
        <div class="container">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-warning custom-flat">
                    <div class="panel-heading custom-flat"><b><span class="glyphicon glyphicon-refresh"
                                                                    aria-hidden="true"></span> Reset Data (Reset Jadwal)</b>
                    </div>
                    <div class="panel-body">
                        <table>
                            <tr>
                                <td>ID</td>
                                <td style="padding-left: 10px">{{ $id }}</td>
                            </tr>
                            <tr>
                                <td>Update Terahir</td>
                                <td style="padding-left: 10px">{{ $data->updated_at }}</td>
                            </tr>
                        </table>
                        <form action={{ url('/admin/reset',$id) }} method="post" name="edit-data" class="horizontal"
                              role='form'>
                            {{ csrf_field() }}
                            <h3>Anda Yakin Akan Mereset Jadwal "{{$id}}" </h3>
                            <br>
                            <a href="{{url('/admin')}}" class="btn btn-default" style="float: left"><span
                                    class="glyphicon glyphicon-menu-left" aria-hidden="true"
                                    style="font-size: 12px"></span>Back</a>
                            {!! Form::button('<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Reset',['class'=>'btn btn-warning','type'=>'submit','style'=>'float:right']) !!}

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </center>


@endsection