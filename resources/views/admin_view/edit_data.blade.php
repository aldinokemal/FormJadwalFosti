@extends('layout.master')

@section('title','Admin | Edit Data')

@section('intro')

@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href={{asset('css/style_login.css')}}>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <center>
        <div class="container">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-primary custom-flat">
                    <div class="panel-heading custom-flat"><b><span class="glyphicon glyphicon-lock"
                                                                    aria-hidden="true"></span> Edit Data (Change
                            Passoword)</b></div>
                    <div class="panel-body">
                        <table>
                            <tr>
                                <td>ID</td>
                                <td style="padding-left: 10px">{{ $id }}</td>
                            </tr>
                            <tr>
                                <td>Update Terahir</td>
                                <td style="padding-left: 10px">{{ $data->updated_at }}</td>
                            </tr>
                        </table>
                        <form action={{ url('/admin/edit',$id) }} method="post" name="edit-data" class="horizontal"
                              role='form'>
                            {{ csrf_field() }}
                            <h3>Masukkan Password Baru </h3><br>
                            @if ($errors->has('new_password'))
                                <div class="form-group has-error" style="margin: 0 0 0 0">
                                    {{ Form::password('new_password',['class'=>'form-control custom-flat', 'placeholder'=>'Enter New Password']) }}
                                </div>
                                <font color='#A52D25' style="float: left;"><span
                                            class="glyphicon glyphicon-exclamation-sign"
                                            aria-hidden="true"></span><strong>{{ $errors->first('new_password') }}</strong></font>
                            @else
                                {{ Form::password('new_password',['class'=>'form-control custom-flat', 'placeholder'=>'Enter New Password']) }}
                            @endif
                            <br><br>
                            <a href={{url('/admin')}} class="btn btn-default" style="float: left"><span
                                    class="glyphicon glyphicon-menu-left" aria-hidden="true"
                                    style="font-size: 12px"></span>Back</a>
                            {!! Form::button('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Change',['class'=>'btn btn-primary','type'=>'submit','style'=>'float:right']) !!}

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </center>


@endsection