@extends('layout.master')

@section('title','Jadwal Fosti | Admin')

@section('intro')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dataTables/datatables.min.css') }}">
    <script src="{{ asset('dataTables/datatables.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#pagination').DataTable({
                order: [2],
                pagingType: "full_numbers",
                responsive: true,
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Cari Data"
                },
                columnDefs: [
                    {orderable: false, targets: [0, -1]}
                ]
            });
        });
    </script>
    <style type="text/css">
        body {
            background: url("{{ asset('images/bg.png')  }}");
        }

    </style>
@endsection

@section('content')
    @if (count($errors) > 0)
        <div style="padding-left:40px; font-size:13px">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br>
    @endif
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style_admin.css') }}">
    <div class="container main" style="margin-top:10px">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="alert alert-info custom-flat">
                    <h2>Data Form Jadwal FOSTI 2016</h2>
                    <small>
                        <span class='glyphicon glyphicon-ok' aria-hidden='true' style='color: green'></span> : Sudah
                        mengirim <br>
                        <span class='glyphicon glyphicon-remove' aria-hidden='true' style='color:red'></span> : Belum
                        mengirim
                    </small>
                </div>

                <div class="table-responsive fresh-datatables">
                    <table border="1" class="table table-striped table-bordered table-hover" id="pagination"
                           width="100%">
                        <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Id</th>
                            <th rowspan="2">Status</th>
                            <th colspan="6">Jadwal</th>
                            <th rowspan="2">Aksi</th>
                        </tr>
                        <tr>
                            <th>Senin</th>
                            <th>Selasa</th>
                            <th>Rabu</th>
                            <th>Kamis</th>
                            <th>Jumat</th>
                            <th>Sabtu</th>
                        </tr>
                        </thead>
                        <?php $no = 1;?>
                        @foreach($datas as $user)
                            @if($user->id != "admin")
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td> {{ $user->id }} </td>
                                    @if($user->status == NULL)
                                        <td><span class='glyphicon glyphicon-remove' aria-hidden='true'
                                                  style='color:red'><span style=" visibility: hidden;">a</span></span>
                                        </td>
                                    @else
                                        <td><span class='glyphicon glyphicon-ok' aria-hidden='true'
                                                  style='color:green'><span style=" visibility: hidden;">b</span></span>
                                        </td>
                                    @endif

                                    @if($user->senin == NULL)
                                        <td>-</td>
                                    @else
                                        <td>{{ $user->senin }}</td>
                                    @endif

                                    @if($user->selasa == NULL)
                                        <td>-</td>
                                    @else
                                        <td>{{ $user->selasa }}</td>
                                    @endif

                                    @if($user->rabu == NULL)
                                        <td>-</td>
                                    @else
                                        <td>{{ $user->rabu }}</td>
                                    @endif

                                    @if($user->kamis == NULL)
                                        <td>-</td>
                                    @else
                                        <td>{{ $user->kamis }}</td>
                                    @endif

                                    @if($user->jumat == NULL)
                                        <td>-</td>
                                    @else
                                        <td>{{ $user->jumat }}</td>
                                    @endif

                                    @if($user->sabtu == NULL)
                                        <td>-</td>
                                    @else
                                        <td>{{ $user->sabtu }}</td>
                                    @endif

                                    <td><a href="{{ url('admin/edit', encrypt($user->id)) }}"
                                           data-tooltip="change password" data-position="top center">
                                           <i class="fa fa-edit" aria-hidden="true"></i></a>
                                        <a href="{{ url('admin/reset', encrypt($user->id)) }}"
                                           data-tooltip="reset" data-position="top center">
                                           <i class="fa fa-repeat" aria-hidden="true"></i>
                                        </a>
                                        <a href="{{ url('admin/delete',encrypt($user->id)) }}"
                                           data-tooltip="remove" data-position="top center">
                                           <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php $no++;?>
                            @endif
                        @endforeach
                    </table>
                </div>
                <span class='glyphicon glyphicon-ok' aria-hidden='true' style='color: green'></span> : {{ $pengirim }}
                <br>
                <span class='glyphicon glyphicon-remove' aria-hidden='true' style='color:red'></span> : {{ $belum }}
                <br><br>
                <form action="{{ action('AdminPostController@logout') }}" method="post" name="logout">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger" style="float: right; width: 100px; margin: 0 0 0 5px">
                        <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>Logout
                    </button>

                </form>

                <div class="btn-group dropup">
                    <button type="button" class="btn btn-default">Action</button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a class="btn btn-default custom-flat" href="{{ url('/admin/insert') }}" style="border:0; "
                               data-tooltip="Insert new data" data-position="right center">
                                <span style="float: left"><i class="fa fa-user-plus"></i></span> <p>Insert</p></a>
                        </li>
                        <li>
                            <a class="btn btn-default custom-flat" style="border:0; " onclick="resetModal();" data-tooltip="Reset all data in database (permanent)" data-position="right center"> <span style="float: left">
                               <i class="fa fa-warning"></i></span>
                                <p>Reset All</p>
                            </a>
                        </li>
                        <li>
                            <a class="btn btn-default open-modal changepasswordModal custom-flat"
                               data-target='#changepasswordModal' data-toggle="modal" style="border:0"
                               data-tooltip="Change admin password" data-position="right center">
                                <span style="float: left"><i class="fa fa-unlock-alt" aria-hidden="true"></i> </span>
                                <p>Password</p>
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{action('ShowJadwalController@semua')}}" target='_blank'
                               class="btn btn-default custom-flat" style="border:0">
                                <span style="float: left"><i class="fa fa-bar-chart"></i></span><p>Chart</p>
                            </a>
                        </li>

                    </ul>
                </div>


            </div>
        </div>
    </div>



<!-- Semantic modal reset -->
<div class="ui basic modal">
  <div class="ui icon header">
    <i class="repeat icon"></i>
    Reset all datas ?
  </div>
  <div class="content">
    <p style="text-align: center;">Your action can't be undone</p>
  </div>
  <div class="actions">
    <div class="ui red basic cancel inverted button">
      <i class="remove icon"></i>
      No
    </div>
    <div class="ui green ok inverted button" onclick="resetConfirmation();">
      <i class="checkmark icon"></i>
      Yes
        <form action="{{ action('AdminPostController@reset_all') }}" method="post" id="form_reset_all">
            {{ csrf_field() }}

        </form>
    </div>
  </div>
</div>


<!-- END -->




    <!-- Modal ChangeAdminPassword -->
    <div class="modal fade" id="changepasswordModal" role="dialog" tabindex="-1" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <form class="form-horizontal" id="form_changepassword">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">Change Admin Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Password baru</label>
                            <div class="col-md-8">
                            <div class="input-group">
                                <input type="password" name="newpassword" class="form-control" id="newpassword" required>
                                <span class="input-group-addon" id="span_eye_icon" onclick="unhidePassword();"><span id="eye_icon" class="glyphicon glyphicon-eye-close" aria-hidden="true"></span></span>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="float:left;" type="button" class="btn btn-primary saveAdminPass"
                                id="saveAdminPass"> Save
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <script>
        function unhidePassword(){
            $("#eye_icon").attr('class', 'glyphicon glyphicon-eye-open');
            $("#span_eye_icon").attr("onclick","hidePassword()");
            $("#newpassword").attr("type","text");
        }
        function hidePassword(){
            $("#eye_icon").attr('class', 'glyphicon glyphicon-eye-close');
            $("#span_eye_icon").attr("onclick","unhidePassword()");
            $("#newpassword").attr("type","password");
        }
    
        function resetModal(){
            $('.ui.basic.modal').modal('show');
        };

        function resetConfirmation() {            
            document.getElementById("form_reset_all").submit();
        };

        $(document).ready(function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
        $(document).ready(function () {
            var _token = $('meta[name="csrf-token"]').attr('content');
            var id = 'admin';
            $('.saveAdminPass').click(function () {
                var newpassword = $('#newpassword').val();
                var $url = '/admin/changeadminpass';

                if (newpassword == '') {
                    swal("Failed", "Password tidak boleh kosong", "error");
                    return;
                }
                if (newpassword.length < 4) {
                    swal("Failed", "Password min 4 karakter", "error");
                    return;
                }

                formData = {
                    Id: id,
                    newpassword: newpassword
                };
                console.log(formData);
                swal({
                        title: "Konfirmasi",
                        text: "Anda yakin akan ganti password ?",
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    },
                    function () {
                        $.ajax({
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': _token},
                            data: formData,
                            dataType: 'json',
                            url: $url,
                            success: function (data) {
                                console.log('berhasil');
                                
                                swal("Success", data.data.message, "success");
                                $('#form_changepassword').trigger("reset");
                                <!-- // $('#changepasswordModal').modal('hide'); -->

                            },
                            error: function () {
                                swal("Failed", "Something Error", "error");
                            }

                        });

                    }); 
            });
        });

    </script>
@endsection