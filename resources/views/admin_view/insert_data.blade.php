@extends('layout.master')

@section('title','Admin | Insert Data')

@section('intro')

@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href={{asset('css/style_login.css')}}>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <center>
        <div class="container">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-success custom-flat">
                    <div class="panel-heading custom-flat"><b><i class="fa fa-user-plus"></i> Insert Data</b></div>
                    <div class="panel-body">

                        {{ Form::open(['url'=>'/admin/insert','class'=>'form-horizontal','role'=>'form','name'=>'form_insert']) }}
                        {{ csrf_field() }}

                        @if ($errors->has('id_insert'))
                            <div class="form-group has-error" style="margin: 0 0 0 0">
                                ID {{ Form::text('id_insert','',['class'=>'form-control custom-flat', 'placeholder'=>'Enter ID']) }}
                            </div>
                            <font color='#A52D25' style="float: left;"><strong><span
                                            class="glyphicon glyphicon-exclamation-sign"
                                            aria-hidden="true"></span> {{ $errors->first('id_insert') }}</strong></font>
                        @else
                            ID
                            {{ Form::text('id_insert','',['class'=>'form-control custom-flat', 'placeholder'=>'Enter ID']) }}
                        @endif
                        <br>
                        <!-- Password Field -->

                        @if ($errors->has('password_insert'))
                            Password
                            <div class="form-group has-error" style="margin: 0 0 0 0">
                                {{ Form::password('password_insert',['class'=>'form-control custom-flat', 'placeholder'=>'Enter Password']) }}
                            </div>
                            <font color='#A52D25' style="float: left;"><strong><span
                                            class="glyphicon glyphicon-exclamation-sign"
                                            aria-hidden="true"></span> {{ $errors->first('password_insert') }}</strong></font>
                        @else
                            Password
                            {{ Form::password('password_insert',['class'=>'form-control custom-flat', 'placeholder'=>'Enter Password']) }}

                        @endif
                        <br>

                        <!-- Confirmation Password -->
                        @if (!$errors->has('password_insert') AND $errors->has('confirmation_password_insert'))
                            <div class="form-group has-error" style="margin: 0 0 0 0">
                                {{ Form::password('confirmation_password_insert',['class'=>'form-control custom-flat', 'placeholder'=>'Enter Confirmation Password']) }}
                            </div>
                            <font color='#A52D25' style="float: left;"><strong><span
                                            class="glyphicon glyphicon-exclamation-sign"
                                            aria-hidden="true"></span> {{ $errors->first('confirmation_password_insert') }}
                                </strong></font>

                        @else
                            {{ Form::password('confirmation_password_insert',['class'=>'form-control custom-flat', 'placeholder'=>'Enter Confirmation Password']) }}
                        @endif

                        <br><br>
                        <a href={{url('/admin')}} class="btn btn-default" style="float: left"> <span
                                class="glyphicon glyphicon-menu-left" aria-hidden="true" style="font-size: 12px"></span>Back</a>
                        {!! Form::button('<i class="fa fa-user-plus"></i> Insert',['class'=>'btn btn-success','type'=>'submit','style'=>'float:right']) !!}

                        {{ Form::close() }}
                        <br><br><br>

                        @if(session()->has('data'))
                            <div class="alert alert-success custom-flat"><font color='#3C763D'><h4>
                                        <b>{{ Session('data') }}</h4></b></font></div>
                        @endif

                        @if($errors->has('DBError'))
                            <div class="alert alert-danger custom-flat"><font color='#A52D25'><h4><b><span
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"
                                                    style="font-size: 16px"></span> {{$errors->first()}}</h4></b></font>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </center>
@endsection