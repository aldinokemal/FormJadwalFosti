@extends('layout.master')

@section('title','Admin | Delete Data')

@section('intro')

@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href={{asset('css/style_login.css')}}>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <center>
        <div class="container">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-danger custom-flat">
                    <div class="panel-heading custom-flat"><b><span class="glyphicon glyphicon-erase"
                                                                    aria-hidden="true"></span> Delete Data</b></div>
                    <div class="panel-body">
                        <form method='post' action={{ url('/admin/delete', $id) }} class="horizontal" role='form'
                              name='form-delete'>
                            {{ csrf_field() }}
                            <table>
                                <tr>
                                    <td>ID</td>
                                    <td style="padding-left: 10px">{{ $id }}</td>
                                </tr>
                                <tr>
                                    <td>Dibuat</td>
                                    <td style="padding-left: 10px">{{ $data->created_at }}</td>
                                </tr>
                                <tr>
                                    <td>Update Terahir</td>
                                    <td style="padding-left: 10px">{{ $data->updated_at }}</td>
                                </tr>
                            </table>
                            <h3>Anda Yakin Akan Menghapus "{{ $id }}" Secara Permanen dari Database ?</h3>
                            <br>
                            <a href="{{url('/admin')}}" class="btn btn-default" style="float: left"><span
                                    class="glyphicon glyphicon-menu-left" aria-hidden="true"
                                    style="font-size: 12px"></span>Back</a>
                            {!! Form::button('<span class="glyphicon glyphicon-erase" aria-hidden="true"></span> Delete',['class'=>'btn btn-danger','type'=>'submit','style'=>'float:right;']) !!}

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </center>



@endsection