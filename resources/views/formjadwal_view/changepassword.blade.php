@extends('layout.master')

@section('title', 'Form Jadwal | Change Password')

@section('intro')
@endsection

@section('content')
    <link rel="stylesheet" type="text/css" href={{asset('css/style_login.css')}}>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <center>
        <div class="container">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-primary custom-flat">
                    <div class="panel-heading custom-flat"><b><span class="glyphicon glyphicon-lock"
                                                                    aria-hidden="true"></span> Change Passoword</b>
                    </div>
                    <div class="panel-body">
                        <form action="{{ action('FormJadwalPostController@changepass') }}" method="post">
                        {{ csrf_field() }}
                        <!-- Current Password -->
                            <font style="float: left;">Current Password</font>
                            @if($errors->has('password_sekarang'))
                                <div class="form-group has-error" style="margin: 0 0 0 0">
                                    <input type="password" name="password_sekarang" class="form-control custom-flat"
                                           placeholder="Enter Current Password">
                                </div>
                                <font color='#A52D25' style="float: left;"><strong><span
                                                class="glyphicon glyphicon-exclamation-sign"
                                                aria-hidden="true"></span> {{ $errors->first('password_sekarang') }}
                                    </strong></font>
                                <!-- No Error -->
                            @elseif(!$errors->has('password_sekarang') and (!$errors->has('notmatch')))
                                <input type="password" name="password_sekarang" class="form-control custom-flat"
                                       placeholder="Enter Current Password">
                                <!-- Wrong Password -->
                            @else($errors->has('notmatch'))
                                <div class="form-group has-error" style="margin: 0 0 0 0">
                                    <input type="password" name="password_sekarang" class="form-control custom-flat"
                                           placeholder="Enter Current Password">
                                </div>
                                <font color='#A52D25' style="float: left;"><strong><span
                                                class="glyphicon glyphicon-exclamation-sign"
                                                aria-hidden="true"></span> {{ $errors->first('notmatch') }}
                                    </strong></font>
                            @endif
                            <br><br>
                            <!-- New Password -->
                            <font style="float: left;">New Password</font>
                            @if($errors->has('password_baru'))
                                <div class="form-group has-error" style="margin: 0 0 0 0">
                                    <input type="password" name="password_baru" class="form-control custom-flat"
                                           placeholder="Enter New Password">
                                </div>
                                <font color='#A52D25' style="float: left;"><strong><span
                                                class="glyphicon glyphicon-exclamation-sign"
                                                aria-hidden="true"></span> {{ $errors->first('password_baru') }}
                                    </strong></font>
                            @else
                                <input type="password" name="password_baru" class="form-control custom-flat"
                                       placeholder="Enter New Password">
                            @endif
                            <br>
                            <!-- Conf New Paswd -->
                            @if($errors->has('password_baru_conf') and !$errors->has('password_baru'))
                                <div class="form-group has-error" style="margin: 0 0 0 0">
                                    <input type="password" name="password_baru_conf" class="form-control custom-flat"
                                           placeholder="Enter Confirmation Password">
                                </div>
                                <font color='#A52D25' style="float: left;"><strong><span
                                                class="glyphicon glyphicon-exclamation-sign"
                                                aria-hidden="true"></span> {{ $errors->first('password_baru_conf') }}
                                    </strong></font>
                            @else
                                <input type="password" name="password_baru_conf" class="form-control custom-flat"
                                       placeholder="Enter Confirmation Password">
                            @endif
                            <br><br>
                            <a href="{{url('/')}}" class="btn btn-default" style="float: left;"><span
                                        class="glyphicon glyphicon-menu-left" aria-hidden="true"
                                        style="font-size: 12px"></span> Back</a>
                            <button class="btn btn-primary" style="float: right;"><i class="fa fa-unlock-alt"></i>
                                Change
                            </button>
                        </form>
                    </div>
                    @if(session()->has('berhasil'))
                        <div class="alert alert-info custom-flat"><font color='#337AB7' style="font-size: 12px">
                                <b> {{ Session('berhasil') }} </b></font>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </center>


@endsection