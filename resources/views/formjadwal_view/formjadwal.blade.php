@extends('layout.master')

@section('title', 'Jadwal Fosti | Form Jadwal')

@section('intro')
    <script>
        $(document).ready(function () {
            $('#table-select').find('td').click(function (event) {
                if (event.target.type !== 'checkbox') {
                    $(':checkbox', this).trigger('click');
                }
            });
        });
    </script>
@endsection

@section('content')
    @if (session()->has('success'))
        <script type="text/javascript">
            window.onload = function () {
                swal("Success!", "{{ session('success') }}", "success");
            };
        </script>

    @endif
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style_form.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=0.45">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{action('ShowJadwalController@semua')}}" target='_blank' style="width: 200px">
                            <i class="fa fa-bar-chart"></i> View Chart
                        </a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                           style="width: 150px; float: right;">
                            {{ session('user') }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/jadwal/changepass',session('user')) }}" style="width: 160px"><span
                                            class="glyphicon glyphicon-lock" aria-hidden="true"
                                            style="font-size: 11px; margin-right: 4px"></span>Change Password</a></li>
                            <li><a href="{{ url('/logout') }}" style="width: 160px"><i class="fa fa-sign-out"
                                                                                       style="margin-right:2px"></i>Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <center>
        <div class="container">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="panel panel-primary custom-flat" style="margin-top:1em">
                    <div class="panel-heading custom-flat"><b><span class="glyphicon glyphicon-calendar"
                                                                    aria-hidden="true"></span> Jadwal Kuliah Anggota
                            FOSTI</b></div>
                    <div class="panel-body">
                        @if ($errors->has('jadwal_error'))
                            <div class="alert alert-danger custom-flat"><h3> {{ $errors->first('jadwal_error') }}</h3>
                            </div>
                        @endif
                        <div class="alert alert-info custom-flat">
                            <b>Jadwal Sekarang</b>
                            <div class="table-responsive" style="overflow-x:auto;">
                                <table border="1" class="table-bordered">
                                    <tr>
                                        <td>ID</td>
                                        <td>Senin</td>
                                        <td>Selasa</td>
                                        <td>Rabu</td>
                                        <td>Kamis</td>
                                        <td>Jumat</td>
                                        <td>Sabtu</td>
                                    </tr>
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        @if ($user->senin == NULL)
                                            <td>-</td>
                                        @else
                                            <td>{{ $user->senin }}</td>
                                        @endif

                                        @if ($user->selasa == NULL)
                                            <td>-</td>
                                        @else
                                            <td>{{ $user->selasa }}</td>
                                        @endif

                                        @if ($user->rabu == NULL)
                                            <td>-</td>
                                        @else
                                            <td>{{ $user->rabu }}</td>
                                        @endif

                                        @if ($user->kamis == NULL)
                                            <td>-</td>
                                        @else
                                            <td>{{ $user->kamis }}</td>
                                        @endif

                                        @if ($user->jumat == NULL)
                                            <td>-</td>
                                        @else
                                            <td>{{ $user->jumat }}</td>
                                        @endif

                                        @if ($user->sabtu == NULL)
                                            <td>-</td>
                                        @else
                                            <td>{{ $user->sabtu }}</td>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <form action={{ url('/jadwal/submit') }} method="post">
                            <div class="form_jadwal_table">
                                {{ csrf_field() }}
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="table-select">
                                        <tr>
                                            <th style="width: 100px">Jam-KE</th>
                                            <th>Senin</th>
                                            <th>Selasa</th>
                                            <th>Rabu</th>
                                            <th>Kamis</th>
                                            <th>Jumat</th>
                                            <th>Sabtu</th>
                                        </tr>

                                        @for($i=1;$i<17;$i++)
                                            <tr>
                                                <th>{{ $i }}</th>
                                                <td><input type='checkbox' name='sen{{$i}}' id='sen{{$i}}' value={{$i}}>
                                                </td>
                                                <td><input type='checkbox' name='sel{{$i}}' id='sel{{$i}}' value={{$i}}>
                                                </td>
                                                <td><input type='checkbox' name='rab{{$i}}' id='rab{{$i}}' value={{$i}}>
                                                </td>
                                                <td><input type='checkbox' name='kam{{$i}}' id='kam{{$i}}' value={{$i}}>
                                                </td>
                                                <td><input type='checkbox' name='jum{{$i}}' id='jum{{$i}}' value={{$i}}>
                                                </td>
                                                <td><input type='checkbox' name='sab{{$i}}' id='sab{{$i}}' value={{$i}}>
                                                </td>
                                            </tr>
                                        @endfor
                                    </table>
                                </div>
                            </div>

                            <button type="button" class="btn btn-primary" style="float: left;" data-toggle="modal"
                                    data-target="#submitnow">
                                <span class="glyphicon glyphicon-send" aria-hidden="true"
                                      style="font-size: 13px"></span>
                                Submit
                            </button>

                            <!-- Start -->
                            <div class="modal fade" id="submitnow" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title" align="center">SUBMIT</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="alert alert-info" role="alert">
                                                <p align="center">Kamu Dapat <b>MENGUPDATE</b> Jadwal Kamu dengan <b>MENGIRIM
                                                        ULANG</b> Jadwal yang Baru<br>Kirim Jadwal Kamu Sekarang ?</p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" name="submit" class="btn btn-primary"
                                                    style="width: 65px">
                                                Ya
                                            </button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal"
                                                    style="width: 65px">Close
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- stop -->
                        </form>
                    </div>
                </div>
            </div>
        </div> <!-- div container -->

    </center>
@endsection