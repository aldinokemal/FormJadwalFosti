<?php


// Route::get('tes', function() {
//     return view('tes');
// });



Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return redirect('/login');
    });

    Route::get('/login', 'LoginController@index');
    Route::post('/login', 'LoginController@proses');

    // Route::get('createuser', 'CreateIdController@createuser');

    // whoareyou ??
    Route::get('whoareyou', 'LoginController@whoareyou');

    // ADMIN
    Route::group(['middleware' => 'admin_privilege'], function () {
        Route::get('/admin', 'AdminGetController@index');
        Route::post('/admin/logout', 'AdminPostController@logout');
        Route::post('/admin/reset_all', 'AdminPostController@reset_all');

        Route::get('/admin/insert', 'AdminGetController@insert');
        ROute::post('/admin/insert', 'AdminPostController@insert');

        Route::get('/admin/edit/{id}', 'AdminGetController@edit');
        Route::post('/admin/edit/{id}', 'AdminPostController@edit_data');

        Route::get('/admin/reset/{id}', 'AdminGetController@reset');
        Route::post('/admin/reset/{id}', 'AdminPostController@reset_data');

        Route::get('/admin/delete/{id}', 'AdminGetController@delete');
        Route::post('/admin/delete/{id}', 'AdminPostController@delete_data');

        Route::get('/admin/delete', 'AdminGetController@delete');
            // admin ajax change password
        Route::post('/admin/changeadminpass', 'AdminPostController@change_admin_password');
    });

    // FORMJADWAL (user)
    Route::group(['middleware' => 'user_privilege'], function () {
        Route::get('/jadwal/{id}', 'FormJadwalGetController@show_form');
        Route::get('/jadwal/changepass/{id}', 'FormJadwalGetController@changepass');
        Route::get('/logout', 'FormJadwalGetController@logout');
        Route::post('/jadwal/submit', 'FormJadwalPostController@submit');
        Route::post('/jadwal/changepass', 'FormJadwalPostController@changepass');
        Route::post('/jadwal/logout', 'FormJadwalPostController@logout');
    });

    // SHOW CHART
    Route::get('/semua', 'ShowJadwalController@semua');
    Route::get('/senin', 'ShowJadwalController@senin');
    Route::get('/selasa', 'ShowJadwalController@selasa');
    Route::get('/rabu', 'ShowJadwalController@rabu');
    Route::get('/kamis', 'ShowJadwalController@kamis');
    Route::get('/jumat', 'ShowJadwalController@jumat');
    Route::get('/sabtu', 'ShowJadwalController@sabtu');

});
