<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

class ShowJadwalController extends Controller
{
    public function semua()
    {
        $jadwal_semuas = DB::table('logins')->select('id', 'status', 'senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu')->get();
        // set nilai awal pada hari
        $total_senin = 0;
        $total_selasa = 0;
        $total_rabu = 0;
        $total_kamis = 0;
        $total_jumat = 0;
        $total_sabtu = 0;

        $total_pengirim = 0;

        foreach ($jadwal_semuas as $jadwal_semua) {
            if ($jadwal_semua->id != 'admin') {
                if ($jadwal_semua->status != NULL) {
                    $total_pengirim += 1;
                }

                $total_senin += sizeof($jadwal_semua->senin);
                $total_selasa += sizeof($jadwal_semua->selasa);
                $total_rabu += sizeof($jadwal_semua->rabu);
                $total_kamis += sizeof($jadwal_semua->kamis);
                $total_jumat += sizeof($jadwal_semua->jumat);
                $total_sabtu += sizeof($jadwal_semua->sabtu);
                // echo var_dump(sizeof($jadwal_semua->rabu))."<br>";
                // echo var_dump($jadwal_semua->rabu);
            }

        }

        return view('jadwal_view/semua')->with(['senin' => $total_selasa,
            'selasa' => $total_selasa,
            'rabu' => $total_rabu,
            'kamis' => $total_kamis,
            'jumat' => $total_jumat,
            'sabtu' => $total_sabtu,
            'total' => $total_pengirim
        ]);
    }

    public function senin()
    {
        $jadwal_semuas = DB::table('logins')->select('id', 'status', 'senin')->get();

        $total_pengirim = 0;

        $ke_1 = 0;
        $ke_2 = 0;
        $ke_3 = 0;
        $ke_4 = 0;
        $ke_5 = 0;
        $ke_6 = 0;
        $ke_7 = 0;
        $ke_8 = 0;
        $ke_9 = 0;
        $ke_10 = 0;
        $ke_11 = 0;
        $ke_12 = 0;
        $ke_13 = 0;
        $ke_14 = 0;
        $ke_15 = 0;
        $ke_16 = 0;

        foreach ($jadwal_semuas as $jadwal_semua) {
            if ($jadwal_semua->id != 'admin') {
                $senin = explode(',', $jadwal_semua->senin);
                if ($jadwal_semua->status != NULL) {
                    $total_pengirim += 1;
                    foreach ($senin as $jam) {
                        if ($jam == "1") {
                            $ke_1 += 1;
                        }
                        if ($jam == "2") {
                            $ke_2 += 1;
                        }
                        if ($jam == "3") {
                            $ke_3 += 1;
                        }
                        if ($jam == "4") {
                            $ke_4 += 1;
                        }
                        if ($jam == "5") {
                            $ke_5 += 1;
                        }
                        if ($jam == "6") {
                            $ke_6 += 1;
                        }
                        if ($jam == "7") {
                            $ke_7 += 1;
                        }
                        if ($jam == "8") {
                            $ke_8 += 1;
                        }
                        if ($jam == "9") {
                            $ke_9 += 1;
                        }
                        if ($jam == "10") {
                            $ke_10 += 1;
                        }
                        if ($jam == "11") {
                            $ke_11 += 1;
                        }
                        if ($jam == "12") {
                            $ke_12 += 1;
                        }
                        if ($jam == "13") {
                            $ke_13 += 1;
                        }
                        if ($jam == "14") {
                            $ke_14 += 1;
                        }
                        if ($jam == "15") {
                            $ke_15 += 1;
                        }
                        if ($jam == "16") {
                            $ke_16 += 1;
                        }
                    }
                }
            }

        }
        return view('/jadwal_view/senin')->with(['ke1' => $ke_1, 'ke2' => $ke_2, 'ke3' => $ke_3, 'ke4' => $ke_4, 'ke5' => $ke_5, 'ke6' => $ke_6, 'ke7' => $ke_7, 'ke8' => $ke_8, 'ke9' => $ke_9, 'ke10' => $ke_10, 'ke11' => $ke_11, 'ke12' => $ke_12, 'ke13' => $ke_13, 'ke14' => $ke_14, 'ke15' => $ke_15, 'ke16' => $ke_16, 'total' => $total_pengirim]);
    }

    public function selasa()
    {
        $jadwal_semuas = DB::table('logins')->select('id', 'status', 'selasa')->get();

        $total_pengirim = 0;

        $ke_1 = 0;
        $ke_2 = 0;
        $ke_3 = 0;
        $ke_4 = 0;
        $ke_5 = 0;
        $ke_6 = 0;
        $ke_7 = 0;
        $ke_8 = 0;
        $ke_9 = 0;
        $ke_10 = 0;
        $ke_11 = 0;
        $ke_12 = 0;
        $ke_13 = 0;
        $ke_14 = 0;
        $ke_15 = 0;
        $ke_16 = 0;

        foreach ($jadwal_semuas as $jadwal_semua) {
            if ($jadwal_semua->id != 'admin') {
                $selasa = explode(',', $jadwal_semua->selasa);
                if ($jadwal_semua->status != NULL) {
                    $total_pengirim += 1;
                    foreach ($selasa as $jam) {
                        if ($jam == "1") {
                            $ke_1 += 1;
                        }
                        if ($jam == "2") {
                            $ke_2 += 1;
                        }
                        if ($jam == "3") {
                            $ke_3 += 1;
                        }
                        if ($jam == "4") {
                            $ke_4 += 1;
                        }
                        if ($jam == "5") {
                            $ke_5 += 1;
                        }
                        if ($jam == "6") {
                            $ke_6 += 1;
                        }
                        if ($jam == "7") {
                            $ke_7 += 1;
                        }
                        if ($jam == "8") {
                            $ke_8 += 1;
                        }
                        if ($jam == "9") {
                            $ke_9 += 1;
                        }
                        if ($jam == "10") {
                            $ke_10 += 1;
                        }
                        if ($jam == "11") {
                            $ke_11 += 1;
                        }
                        if ($jam == "12") {
                            $ke_12 += 1;
                        }
                        if ($jam == "13") {
                            $ke_13 += 1;
                        }
                        if ($jam == "14") {
                            $ke_14 += 1;
                        }
                        if ($jam == "15") {
                            $ke_15 += 1;
                        }
                        if ($jam == "16") {
                            $ke_16 += 1;
                        }
                    }
                }
            }

        }
        return view('/jadwal_view/selasa')->with(['ke1' => $ke_1, 'ke2' => $ke_2, 'ke3' => $ke_3, 'ke4' => $ke_4, 'ke5' => $ke_5, 'ke6' => $ke_6, 'ke7' => $ke_7, 'ke8' => $ke_8, 'ke9' => $ke_9, 'ke10' => $ke_10, 'ke11' => $ke_11, 'ke12' => $ke_12, 'ke13' => $ke_13, 'ke14' => $ke_14, 'ke15' => $ke_15, 'ke16' => $ke_16, 'total' => $total_pengirim]);
    }

    public function rabu()
    {
        $jadwal_semuas = DB::table('logins')->select('id', 'status', 'rabu')->get();

        $total_pengirim = 0;

        $ke_1 = 0;
        $ke_2 = 0;
        $ke_3 = 0;
        $ke_4 = 0;
        $ke_5 = 0;
        $ke_6 = 0;
        $ke_7 = 0;
        $ke_8 = 0;
        $ke_9 = 0;
        $ke_10 = 0;
        $ke_11 = 0;
        $ke_12 = 0;
        $ke_13 = 0;
        $ke_14 = 0;
        $ke_15 = 0;
        $ke_16 = 0;

        foreach ($jadwal_semuas as $jadwal_semua) {
            if ($jadwal_semua->id != 'admin') {
                $rabu = explode(',', $jadwal_semua->rabu);
                if ($jadwal_semua->status != NULL) {
                    $total_pengirim += 1;
                    foreach ($rabu as $jam) {
                        if ($jam == "1") {
                            $ke_1 += 1;
                        }
                        if ($jam == "2") {
                            $ke_2 += 1;
                        }
                        if ($jam == "3") {
                            $ke_3 += 1;
                        }
                        if ($jam == "4") {
                            $ke_4 += 1;
                        }
                        if ($jam == "5") {
                            $ke_5 += 1;
                        }
                        if ($jam == "6") {
                            $ke_6 += 1;
                        }
                        if ($jam == "7") {
                            $ke_7 += 1;
                        }
                        if ($jam == "8") {
                            $ke_8 += 1;
                        }
                        if ($jam == "9") {
                            $ke_9 += 1;
                        }
                        if ($jam == "10") {
                            $ke_10 += 1;
                        }
                        if ($jam == "11") {
                            $ke_11 += 1;
                        }
                        if ($jam == "12") {
                            $ke_12 += 1;
                        }
                        if ($jam == "13") {
                            $ke_13 += 1;
                        }
                        if ($jam == "14") {
                            $ke_14 += 1;
                        }
                        if ($jam == "15") {
                            $ke_15 += 1;
                        }
                        if ($jam == "16") {
                            $ke_16 += 1;
                        }
                    }
                }
            }

        }
        return view('/jadwal_view/rabu')->with(['ke1' => $ke_1, 'ke2' => $ke_2, 'ke3' => $ke_3, 'ke4' => $ke_4, 'ke5' => $ke_5, 'ke6' => $ke_6, 'ke7' => $ke_7, 'ke8' => $ke_8, 'ke9' => $ke_9, 'ke10' => $ke_10, 'ke11' => $ke_11, 'ke12' => $ke_12, 'ke13' => $ke_13, 'ke14' => $ke_14, 'ke15' => $ke_15, 'ke16' => $ke_16, 'total' => $total_pengirim]);
    }

    public function kamis()
    {
        $jadwal_semuas = DB::table('logins')->select('id', 'status', 'kamis')->get();

        $total_pengirim = 0;

        $ke_1 = 0;
        $ke_2 = 0;
        $ke_3 = 0;
        $ke_4 = 0;
        $ke_5 = 0;
        $ke_6 = 0;
        $ke_7 = 0;
        $ke_8 = 0;
        $ke_9 = 0;
        $ke_10 = 0;
        $ke_11 = 0;
        $ke_12 = 0;
        $ke_13 = 0;
        $ke_14 = 0;
        $ke_15 = 0;
        $ke_16 = 0;

        foreach ($jadwal_semuas as $jadwal_semua) {
            if ($jadwal_semua->id != 'admin') {
                $kamis = explode(',', $jadwal_semua->kamis);
                if ($jadwal_semua->status != NULL) {
                    $total_pengirim += 1;
                    foreach ($kamis as $jam) {
                        if ($jam == "1") {
                            $ke_1 += 1;
                        }
                        if ($jam == "2") {
                            $ke_2 += 1;
                        }
                        if ($jam == "3") {
                            $ke_3 += 1;
                        }
                        if ($jam == "4") {
                            $ke_4 += 1;
                        }
                        if ($jam == "5") {
                            $ke_5 += 1;
                        }
                        if ($jam == "6") {
                            $ke_6 += 1;
                        }
                        if ($jam == "7") {
                            $ke_7 += 1;
                        }
                        if ($jam == "8") {
                            $ke_8 += 1;
                        }
                        if ($jam == "9") {
                            $ke_9 += 1;
                        }
                        if ($jam == "10") {
                            $ke_10 += 1;
                        }
                        if ($jam == "11") {
                            $ke_11 += 1;
                        }
                        if ($jam == "12") {
                            $ke_12 += 1;
                        }
                        if ($jam == "13") {
                            $ke_13 += 1;
                        }
                        if ($jam == "14") {
                            $ke_14 += 1;
                        }
                        if ($jam == "15") {
                            $ke_15 += 1;
                        }
                        if ($jam == "16") {
                            $ke_16 += 1;
                        }
                    }
                }
            }

        }
        return view('/jadwal_view/kamis')->with(['ke1' => $ke_1, 'ke2' => $ke_2, 'ke3' => $ke_3, 'ke4' => $ke_4, 'ke5' => $ke_5, 'ke6' => $ke_6, 'ke7' => $ke_7, 'ke8' => $ke_8, 'ke9' => $ke_9, 'ke10' => $ke_10, 'ke11' => $ke_11, 'ke12' => $ke_12, 'ke13' => $ke_13, 'ke14' => $ke_14, 'ke15' => $ke_15, 'ke16' => $ke_16, 'total' => $total_pengirim]);
    }

    public function jumat()
    {
        $jadwal_semuas = DB::table('logins')->select('id', 'status', 'jumat')->get();

        $total_pengirim = 0;

        $ke_1 = 0;
        $ke_2 = 0;
        $ke_3 = 0;
        $ke_4 = 0;
        $ke_5 = 0;
        $ke_6 = 0;
        $ke_7 = 0;
        $ke_8 = 0;
        $ke_9 = 0;
        $ke_10 = 0;
        $ke_11 = 0;
        $ke_12 = 0;
        $ke_13 = 0;
        $ke_14 = 0;
        $ke_15 = 0;
        $ke_16 = 0;

        foreach ($jadwal_semuas as $jadwal_semua) {
            if ($jadwal_semua->id != 'admin') {
                $jumat = explode(',', $jadwal_semua->jumat);
                if ($jadwal_semua->status != NULL) {
                    $total_pengirim += 1;
                    foreach ($jumat as $jam) {
                        if ($jam == "1") {
                            $ke_1 += 1;
                        }
                        if ($jam == "2") {
                            $ke_2 += 1;
                        }
                        if ($jam == "3") {
                            $ke_3 += 1;
                        }
                        if ($jam == "4") {
                            $ke_4 += 1;
                        }
                        if ($jam == "5") {
                            $ke_5 += 1;
                        }
                        if ($jam == "6") {
                            $ke_6 += 1;
                        }
                        if ($jam == "7") {
                            $ke_7 += 1;
                        }
                        if ($jam == "8") {
                            $ke_8 += 1;
                        }
                        if ($jam == "9") {
                            $ke_9 += 1;
                        }
                        if ($jam == "10") {
                            $ke_10 += 1;
                        }
                        if ($jam == "11") {
                            $ke_11 += 1;
                        }
                        if ($jam == "12") {
                            $ke_12 += 1;
                        }
                        if ($jam == "13") {
                            $ke_13 += 1;
                        }
                        if ($jam == "14") {
                            $ke_14 += 1;
                        }
                        if ($jam == "15") {
                            $ke_15 += 1;
                        }
                        if ($jam == "16") {
                            $ke_16 += 1;
                        }
                    }
                }
            }

        }
        return view('/jadwal_view/jumat')->with(['ke1' => $ke_1, 'ke2' => $ke_2, 'ke3' => $ke_3, 'ke4' => $ke_4, 'ke5' => $ke_5, 'ke6' => $ke_6, 'ke7' => $ke_7, 'ke8' => $ke_8, 'ke9' => $ke_9, 'ke10' => $ke_10, 'ke11' => $ke_11, 'ke12' => $ke_12, 'ke13' => $ke_13, 'ke14' => $ke_14, 'ke15' => $ke_15, 'ke16' => $ke_16, 'total' => $total_pengirim]);
    }

    public function sabtu()
    {
        $jadwal_semuas = DB::table('logins')->select('id', 'status', 'sabtu')->get();

        $total_pengirim = 0;

        $ke_1 = 0;
        $ke_2 = 0;
        $ke_3 = 0;
        $ke_4 = 0;
        $ke_5 = 0;
        $ke_6 = 0;
        $ke_7 = 0;
        $ke_8 = 0;
        $ke_9 = 0;
        $ke_10 = 0;
        $ke_11 = 0;
        $ke_12 = 0;
        $ke_13 = 0;
        $ke_14 = 0;
        $ke_15 = 0;
        $ke_16 = 0;

        foreach ($jadwal_semuas as $jadwal_semua) {
            if ($jadwal_semua->id != 'admin') {
                $sabtu = explode(',', $jadwal_semua->sabtu);
                if ($jadwal_semua->status != NULL) {
                    $total_pengirim += 1;
                    foreach ($sabtu as $jam) {
                        if ($jam == "1") {
                            $ke_1 += 1;
                        }
                        if ($jam == "2") {
                            $ke_2 += 1;
                        }
                        if ($jam == "3") {
                            $ke_3 += 1;
                        }
                        if ($jam == "4") {
                            $ke_4 += 1;
                        }
                        if ($jam == "5") {
                            $ke_5 += 1;
                        }
                        if ($jam == "6") {
                            $ke_6 += 1;
                        }
                        if ($jam == "7") {
                            $ke_7 += 1;
                        }
                        if ($jam == "8") {
                            $ke_8 += 1;
                        }
                        if ($jam == "9") {
                            $ke_9 += 1;
                        }
                        if ($jam == "10") {
                            $ke_10 += 1;
                        }
                        if ($jam == "11") {
                            $ke_11 += 1;
                        }
                        if ($jam == "12") {
                            $ke_12 += 1;
                        }
                        if ($jam == "13") {
                            $ke_13 += 1;
                        }
                        if ($jam == "14") {
                            $ke_14 += 1;
                        }
                        if ($jam == "15") {
                            $ke_15 += 1;
                        }
                        if ($jam == "16") {
                            $ke_16 += 1;
                        }
                    }
                }
            }

        }
        return view('/jadwal_view/sabtu')->with(['ke1' => $ke_1, 'ke2' => $ke_2, 'ke3' => $ke_3, 'ke4' => $ke_4, 'ke5' => $ke_5, 'ke6' => $ke_6, 'ke7' => $ke_7, 'ke8' => $ke_8, 'ke9' => $ke_9, 'ke10' => $ke_10, 'ke11' => $ke_11, 'ke12' => $ke_12, 'ke13' => $ke_13, 'ke14' => $ke_14, 'ke15' => $ke_15, 'ke16' => $ke_16, 'total' => $total_pengirim]);
    }
}
