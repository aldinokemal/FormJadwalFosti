<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Models\User;

use App\Http\Requests;
use Input, Auth, Validator, Redirect, Hash, Session, Cache;
use App\Login;


class LoginController extends Controller
{
    public function index(Request $request)
    {
        if (Cache::has('suspen_')) {
            if ($request->ip() == Cache::get('ip_tersimpan')) {
                // dd(Cache::get('ip_tersimpan'));
                return redirect('/whoareyou?');
            }
        } else {
            if (Session()->has('admin')) {
                return redirect('admin');
            }
            if (Session()->has('user')) {
                return redirect(url('jadwal', session('user')));
            }
        }
        return view('login');

    }

    public function proses(Request $request)
    {

        $this->validate($request, [
            'user' => 'required|max:10',
            'pwd' => 'required',
        ]);

        $user_data = ['id' => $request['user'],
            'password' => $request['pwd'],
        ];

        // try to login
        if (Auth::attempt($user_data)) {
            if (strtolower($request['user']) === 'admin') {
                Session::put('admin', Auth::user()->id);
                Cache::forget('ip_tersimpan');
                return redirect('admin');
            } else {
                // redirect ke home jika sudah login
                Session::put('user', Auth::user()->id);
                Cache::forget('ip_tersimpan');
                return redirect(url('jadwal', $request['user']));
            }

        } else {
            // untuk mencegah bruteforce
            if (Cache::has('ip_tersimpan')) {
                $coba_login = Cache::get('coba_login');
                // dd($coba_login);
                if ($coba_login < 4) {
                    $coba_login = $coba_login + 1;
                    Cache::put('coba_login', $coba_login, 1);
                } else {
                    Cache::put('suspen_', $request->ip(), 1);
                    return redirect('admin');
                }

            } else {
                $ip = $request->ip();
                // ip tersimpan
                Cache::put('ip_tersimpan', $ip, 1);
                Cache::put('coba_login', 1, 1);
            }


            return redirect('login')->withError('ID atau Password Salah')->withInput($request->except("password"));
        }
        return redirect()->back();
    }

    public function whoareyou()
    {
        if (Cache::has('suspen_')) {
            return view('whoareyou');
        }
        return view('errors.404');
    }
}
