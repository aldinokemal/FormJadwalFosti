<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator, DB, Hash;

use App\Login;

class AdminPostController extends Controller
{

    public function logout()
    {
        Session()->flush();
        return redirect()->back();
    }

    public function insert(Request $request)
    {
        $rules = [
            'id_insert' => 'required|max:10',
            'password_insert' => 'required',
            'confirmation_password_insert' => 'required|same:password_insert',
        ];

        $messages = array(
            'id_insert.required' => 'ID tidak boleh kosong',
            'id_insert.max' => 'ID maksimal 10 digit',
            'password_insert.required' => 'Password tidak boleh kosong',
            'confirmation_password_insert.required' => 'Password konfirmasi tidak boleh kosong',
            'confirmation_password_insert.same' => 'Password tidak sama',
        );

        $this->validate($request, $rules, $messages);

        $user = new Login();
        $user->id = $request['id_insert'];
        $user->password = Hash::make($request['password_insert']);
        try {
            $user->save();
            return redirect('/admin/insert')->with(['data' => 'Berhasil Ditambah']);
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->back()->withErrors(['DBError' => 'ID Telah Terpakai']);

        }

    }

    public function delete_data($id)
    {
        DB::table('logins')->where('id', '=', $id)->delete();
        return redirect('/admin');
    }

    public function edit_data(Request $request, $id)
    {
        $rules = [
            'new_password' => 'required',
        ];
        $messsages = array(
            'new_password.required' => 'Password tidak boleh kosong',
        );

        $this->validate($request, $rules, $messsages);
        // dd(date('Y-m-d H:i:s'));
        DB::table('logins')
            ->where('id', $id)
            ->update(['password' => Hash::make($request['new_password']),
                'updated_at' => date('Y-m-d H:i:s')]);

        return redirect('/admin');
    }

    public function reset_data($id)
    {
        DB::table('logins')
            ->where('id', $id)
            ->update(['status' => NULL,
                'senin' => NULL,
                'selasa' => NULL,
                'rabu' => NULL,
                'kamis' => NULL,
                'jumat' => NULL,
                'sabtu' => NULL]);
        return redirect('/admin');
    }

    public function reset_all()
    {
        $users = DB::table('logins')->orderBy('id')->get();
        foreach ($users as $user) {
            if ($user->id != 'admin') {
                DB::table('logins')
                    ->where('id', $user->id)
                    ->update(['status' => NULL,
                        'senin' => NULL,
                        'selasa' => NULL,
                        'rabu' => NULL,
                        'kamis' => NULL,
                        'jumat' => NULL,
                        'sabtu' => NULL,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }
        }
        return redirect('/admin');
    }

    public function change_admin_password(Request $request)
    {   
        // dd($request);
        DB::table('logins')
            ->where('Id', $request['Id'])
            ->update([
                'password' => bcrypt($request['newpassword'])
                ]);

        
        return response()->json([
            'success'  => true,
            'data'     => ['message'=>'Password successfully changed']
        ],200);
    }
}
