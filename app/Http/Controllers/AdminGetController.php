<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Login;
use Illuminate\Contracts\Encryption\DecryptException;
use DB, Session;

class AdminGetController extends Controller
{
    public function index()
    {    
        // DATABASE 1 (logins)
        $datas = DB::table('logins')->orderBy('id')->get();

        // DATABASE

        // echo session('admin');
        $pengirim = 0;
        foreach ($datas as $data) {
            if ($data->status != NULL || $data->status != "") {
                $pengirim += 1;
            }
        }

        $belum = count($datas) - $pengirim - 1;
        return view('/admin_view/admin')->with(['datas' => $datas, 'pengirim' => $pengirim, 'belum' => $belum]);
        
    }

    public function insert()
    {
        return view('/admin_view/insert_data');
    }

    public function edit($id)
    {           
        try {
            $id = decrypt($id);            
        } catch (DecryptException $e) {
            return abort(404);
        }            
        $single = Login::findOrFail($id);
        return view('/admin_view/edit_data')->with(['data' => $single, 'id' => $id]);
    }

    public function delete($id)
    {
        try {
            $id = decrypt($id);
        } catch (DecryptException $e) {
            return abort(404);
        }
        $single = Login::findOrFail($id);
        return view('/admin_view/delete_data')->with(['data' => $single, 'id' => $id]);
    }

    public function reset($id)
    {
        try {
            $id = decrypt($id);
        } catch (DecryptException $e) {
            return abort(404);
        }        
        $single = Login::findOrFail($id);
        return view('/admin_view/reset_data')->with(['data' => $single, 'id' => $id]);
        
    }
}