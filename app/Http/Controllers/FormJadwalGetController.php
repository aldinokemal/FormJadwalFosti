<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

class FormJadwalGetController extends Controller
{
    public function show_form($id)
    {
        if (session('user') == $id) {
            $user = DB::table('logins')->where('id', $id)->first();
            if (is_null($user)) {
                Session()->flush();
                return redirect('/');
            } else {
                return view('formjadwal_view/formjadwal')->with(['user' => $user]);
            }
        } 
            
        return redirect('/login');
        
    }

    public function logout()
    {
        $id_old = session('user');
        Session()->flush();
        return redirect("/jadwal/$id_old");
    }

    public function changepass($id)
    {
        if (session('user') == $id) {
            return view('formjadwal_view/changepassword');
        } else {
            return redirect()->back();
        }
    }
}
