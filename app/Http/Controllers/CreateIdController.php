<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB, Hash;
use App\Login;

class CreateIdController extends Controller
{
    public function createuser()
    {
        $users = DB::connection('mysql_2');

        $datas = $users->table('pendaftarans')->get();
        // $rest = substr("abcdef", -3);
        // echo $rest;
        // dd($a);
        foreach ($datas as $data) {
            // echo "<br>".$data->nim." => ".substr($data->nim, -3);
            $user = new Login();
            $user->id = $data->nim;
            $user->password = Hash::make(substr($data->nim, -3));
            // dd($user);
            $user->save();

            try {
                $user->save();
                // return redirect('/');
            } catch (\Illuminate\Database\QueryException $e) {
                return redirect()->back()->withErrors(['DBError' => 'ID Telah Terpakai']);

            }
        }
        return redirect('/');
    }

}
