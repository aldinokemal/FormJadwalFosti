<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Input, Session, DB, Hash;

class FormJadwalPostController extends Controller
{
    public function submit(Request $request)
    {
        $jam_senin = [];
        $jam_selasa = [];
        $jam_rabu = [];
        $jam_kamis = [];
        $jam_jumat = [];
        $jam_sabtu = [];

        for ($i = 1; $i < 17; $i++) {
            if ($request->has("sen$i")) {
                array_push($jam_senin, $request["sen$i"]);
            }
            if ($request->has("sel$i")) {
                array_push($jam_selasa, $request["sel$i"]);
            }
            if ($request->has("rab$i")) {
                array_push($jam_rabu, $request["rab$i"]);
            }
            if ($request->has("kam$i")) {
                array_push($jam_kamis, $request["kam$i"]);
            }
            if ($request->has("jum$i")) {
                array_push($jam_jumat, $request["jum$i"]);
            }
            if ($request->has("sab$i")) {
                array_push($jam_sabtu, $request["sab$i"]);
            }
        }

        $data_list_senin = implode(",", $jam_senin);
        $data_list_selasa = implode(",", $jam_selasa);
        $data_list_rabu = implode(",", $jam_rabu);
        $data_list_kamis = implode(",", $jam_kamis);
        $data_list_jumat = implode(",", $jam_jumat);
        $data_list_sabtu = implode(",", $jam_sabtu);

        if ($data_list_senin == "") {
            $data_list_senin = NULL;
        }
        if ($data_list_selasa == "") {
            $data_list_selasa = NULL;
        }
        if ($data_list_rabu == "") {
            $data_list_rabu = NULL;
        }
        if ($data_list_kamis == "") {
            $data_list_kamis = NULL;
        }
        if ($data_list_kamis == "") {
            $data_list_kamis = NULL;
        }
        if ($data_list_jumat == "") {
            $data_list_jumat = NULL;
        }
        if ($data_list_sabtu == "") {
            $data_list_sabtu = NULL;
        }

        $total = sizeof($jam_senin) + sizeof($jam_selasa) + sizeof($jam_rabu) + sizeof($jam_kamis) + sizeof($jam_jumat) + sizeof($jam_sabtu);


        if ($total < 16 OR $total > 28) {
            return redirect()->back()->withErrors(['jadwal_error' => 'Mohon Isikan Jadwal dengan Benar']);
        }

        $sesion = Session('user');
        DB::table('logins')
            ->where('id', $sesion)
            ->update(['status' => "Sudah",
                'senin' => $data_list_senin,
                'selasa' => $data_list_selasa,
                'rabu' => $data_list_rabu,
                'kamis' => $data_list_kamis,
                'jumat' => $data_list_jumat,
                'sabtu' => $data_list_sabtu,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        return redirect()->back()->withSuccess('Jadwal berhasil diperbarui');
    }

    public function changepass(Request $request)
    {
        //BUG TOKENMISMATCH

        $rules = [
            'password_sekarang' => 'required',
            'password_baru' => 'required',
            'password_baru_conf' => 'required|same:password_baru'
        ];

        $messages = [
            'password_sekarang.required' => 'Current Password tidak boleh kosong',
            'password_baru.required' => 'New Password tidak boleh kosong',
            'password_baru_conf.required' => 'Confirmation Password tidak boleh kosong',
            'password_baru_conf.same' => 'New Password dan Confimation Password harus sama'
        ];

        $this->validate($request, $rules, $messages);

        $old_pass = DB::table('logins')->select('password')->where('id', Session('user'))->first();

        /////////////////////////////////
        if (is_null($old_pass)) {
            Session()->flush();
            return redirect('/');
        }
        /////////////////////////////////

        if (Hash::check($request['password_sekarang'], $old_pass->password)) {
            DB::table('logins')
                ->where('id', Session('user'))
                ->update(['password' => Hash::make($request['password_baru']),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            return redirect()->back()->with(['berhasil' => 'Password Berhasil Diperbarui']);
        } else {
            return redirect()->back()->withErrors(['notmatch' => 'Password Salah']);
        }
    }


}
