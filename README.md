# Fosti Jadwal #

## Langkah development
1. git clone project ini / download
2. masuk lokasi folder via terminal / cmd
3. rename **.env.example** => **.env**
5. setting *database*, *username*, dan *password*
6. lakukan `composer install`
7. lakukan `php artisan migrate --seed`
8. jalankan `php artisan serve`
9. buka `127.0.0.1:8000`

## Note
* `php artisan migrate --seed` : membuat database sekaligus insert data


## Login Page
!["Login"](http://preview.ibb.co/mWfdf6/Screenshot_from_2017_11_16_14_53_31.png "login")

## Statistik Page
!["Statistik Page"](http://image.ibb.co/bF94L6/Screenshot_from_2017_11_16_14_57_33.png "Statistik Page")

## Form Page
!["Form Page"](http://image.ibb.co/jts27m/Screenshot_from_2017_11_16_14_55_23.png "Form Page")