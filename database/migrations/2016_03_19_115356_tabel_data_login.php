<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class TabelDataLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logins', function (Blueprint $table) {
            $table->string('id',10);
            $table->string('password');
            $table->primary('id');
            $table->string('status',10)->nullable();
            $table->string('senin',50)->nullable();
            $table->string('selasa',50)->nullable();
            $table->string('rabu',50)->nullable();
            $table->string('kamis',50)->nullable();
            $table->string('jumat',50)->nullable();
            $table->string('sabtu',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logins');
    }
}
