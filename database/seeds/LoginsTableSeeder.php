<?php

use Illuminate\Database\Seeder;

class LoginsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logins')->insert([
        		[
	            'id' => 'admin',
	            'password' => bcrypt('fosti2104'),
	            'updated_at' => date('Y-m-d H:i:s'),
	            'created_at' => date('Y-m-d H:i:s'),
        		],
        		[        	
	        	'id' => 'user',
	            'password' => bcrypt('user'),
	            'updated_at' => date('Y-m-d H:i:s'),
	            'created_at' => date('Y-m-d H:i:s'),
        		],        	
        	]);
    }
}
